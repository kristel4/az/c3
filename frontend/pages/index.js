import Head from 'next/head';
import {Container} from 'react-bootstrap'
import styles from '../styles/Home.module.css';
import Banner from '../components/Banner';
import View from '../components/View';

export default function Home() {
  return (

  	<View title={"Budget Tracker"}>
	  	<Container>
	    	<Banner />
	    </Container>
    </View>
  )
}
