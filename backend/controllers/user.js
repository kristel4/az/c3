const User = require('../models/user');
const {OAuth2Client} = require('google-auth-library')
const bcrypt = require('bcryptjs');
const auth = require('../auth');
const clientId = '655122494220-7glallt06k5l8b8gnqtgosjhte7m3om5.apps.googleusercontent.com';

//for registration

module.exports.register = (params) => {
	let user = new User({
		firstName: params.firstName,
		lastName: params.lastName,
		email: params.email,
		password: bcrypt.hashSync(params.password,10),
		mobileNo: params.mobileNo,
		loginType: params.loginType
	})

	return user.save().then((user, err) => {
		return (err) ? false: true
	})
}

//to check if email already exists

module.exports.emailExists = (params) => {
	return User.find({email: params.email}).then(
		result => {return result.length > 0 ? true : false 	})
} 

// for login
module.exports.login = (params) => {
	return User.findOne({email: params.email}).then(user => {
		if(user === null){
			return {error: 'does-not-exist'}
		}

		if(user.loginType !== 'email' ){
			return {error: 'login-type-error'}
		}

		const isPasswordMatched = bcrypt.compareSync(params.password, user.password)
		if (isPasswordMatched){
			return {accessToken: auth.createAccessToken(user.toObject())}
		} else {
			return {error: 'incorrect-password'}
		}
	})

}

//for finding a user by ID

module.exports.get = (params) => {
	return User.findById(params.userId).then(user => {
		user.password = undefined
		return user
	})
}

//google verification of token 

module.exports.verifyGoogleTokenId = async (tokenId) => {

	const client = new OAuth2Client(clientId);
	const data = await client.verifyIdToken({
		idToken: tokenId,
		audience: clientId
	});

	console.log(data)
	console.log(data.payload.email_verified)

	if(data.payload.email_verified === true){

		//check dito yung details nung email, hanapin sa dataBASE
		const user = await User.findOne({email: data.payload.email});

		if (user!== null){
			// if merong user, check yung account if nakaregister sya as a gmail account
			if(user.loginType === "google"){
				return {accessToken: auth.createAccessToken(user.toObject())}
			} else {
				// if yung nahanap na email is nakaregister pero hindi naka google email
				return {error: "login-type-error"}
			}
		} else {
			// Dito if, walang nahanap na email sa mongodb, ireregister na sya, kunin na yung data sa paylad 

			let user = new User ({
				firstName: data.payload.given_name,
				lastName: data.payload.family_name,
				email: data.payload.email,
				loginType: 'google'
			})

			return user.save().then((user, err) => {return {acccessToken: auth.createAccessToken(user.toObject())}
			})
		}

	} else {
		return{error: "google-auth-error"}
	}
}

//For updating categories array in users

module.exports.addCategory = (params) => {
	console.log(params)
	return User.findById(params.userId).then( user => {
		user.categories.push({
		name: params.categoryName,
		type: params.categoryType})
	

	console.log(user)

	return user.save().then((user,err) => {
		return (err) ? false : true
		})

	})
// let user = await User.findById(params.userId);
	
	// user.categories.push({
	// 	name: params.categoryName,
	// 	type: params.categoryType
	// });

	// try{
	// 	console.log(user);
	// 	await user.save();
	// 	return true;
	// } catch(err){
	// 	return false;
	// }
}


//for updating total balance based on added amount

module.exports.updateBalance = async (params) => {
	console.log(2)
	let amount = parseInt(params.amount)
	const user = await User.findById(params.userId);
	let currentBalance = user.totalBalance;

	console.log(currentBalance)
	console.log(amount)
	console.log(typeof(currentBalance))
	console.log(typeof(amount))
	if (params.categoryType == "Income"){
		currentBalance += amount
	} else if (params.categoryType == "Expense"){
		currentBalance -= amount
	}

	const updates = {
		totalBalance: currentBalance
	}
	
	return User.findByIdAndUpdate(params.userId, updates).then((update,err) => { return(err) ? false : true })
}

//for adding records
module.exports.addRecord = (params) => {
	
	return User.findById(params.userId).then(user => {
		let totalNumber = 0;
		if (params.categoryType == "Income"){
			totalNumber = user.totalBalance+parseInt(params.amount)
		} else if (params.categoryType=="Expense"){
			totalNumber = user.totalBalance-parseInt(params.amount)
		}
		
		user.records.push({
			categoryType: params.categoryType,
			categoryName: params.categoryList,
			amount: params.amount,
			currentBalance: totalNumber,
			description: params.description
		})

		return user.save().then((user,err) => {
			return(err) ? false : true
		})
	})
}

//retrieving data of a user

module.exports.getUser = (params) => {
	
	return User.findById(params.userId).then(user => user)
}

