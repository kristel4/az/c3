import {Fragment, useState, useEffect, useContext} from 'react';
import {Table, Button, Container} from 'react-bootstrap';
import Link from 'next/link';
import View from '../../components/View';
import UserContext from '../../UserContext';

export default function index(){
	
	const {user} = useContext(UserContext);
    const [categories, setCategories] = useState([])
	const [count, setCount] = useState(0)

    useEffect(() => {
    	console.log(user)
  	
  	setCount(1)
    	if(count == 1){
    		console.log('here')
    		if(user === null ){
                setCategories(<Spinner animation="border" />)
            } else if(user!== null){
    			user.categories.reverse()
    			setCategories(user.categories.map(category => {
    				return(
    					<tr key={category._id}>
    						<td>{category.name}</td>
    						<td>{category.type}</td>
    					</tr>
    				)
    			}))
    		}
    	}
        let num = parseInt(count) + 1
    	setCount(num);

    },[user.categories,count,user])

	return(
		
		<View title={"Categories"} >
        <Container>
			<Fragment>
				<h1>Categories</h1>
				<Button href="/categories/new" variant="dark">Add</Button>
					<Table striped bordered hober>
						<thead>
							<tr>
							<th>Category</th>
							<th>Type</th>
							</tr>
						</thead>

						<tbody>
							{categories}
						</tbody>
						
					</Table>
        	</Fragment>
        </Container>
		</View>
	
	)
}

