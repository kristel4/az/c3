import {Fragment, useState, useEffect, useContext} from 'react';
import { Form, Row, Col} from 'react-bootstrap';
import Link from 'next/link';
import View from '../../components/View';
import UserContext from '../../UserContext';
import Records from '../../components/Records';
import moment from 'moment';
import PieChart from '../../components/PieChart'

export default function index(){
	

	const {user} = useContext(UserContext);

    const [fromDate, setFromDate] = useState('2021-04-01');
    const [toDate, setToDate] = useState(moment().format('YYYY-MM-DD'));
    const [perCategoryName, setPerCategoryName] = useState([]);
   	const [amountByCat, setAmountByCat] = useState([])
    
    useEffect(() => {

    let categName = []
  
    	user.records.forEach(record => {
    		if(moment(record.dateAdded).isSameOrAfter(fromDate)){
    			
	    		if(moment(record.dateAdded).isSameOrBefore(toDate)){
	    			 
	    			if(!categName.find(categ => categ === record.categoryName)){
	    				categName.push(record.categoryName)
	    			}

	    		}
    		}
    	})


    	setPerCategoryName(categName);
    	console.log(categName)

    },[user,fromDate,toDate])

    console.log(perCategoryName);

    useEffect(() => {

    	setAmountByCat(perCategoryName.map( perCateg => {

    		let amount = 0;

    		user.records.forEach(record => {
    			if(moment(record.dateAdded).isSameOrAfter(fromDate)){

    				if(moment(record.dateAdded).isSameOrBefore(toDate)){
    					if(record.categoryName === perCateg){
    						amount += parseInt(record.amount)
    					}
    				}
    			}
    		})

    		return {
    			categoryName: perCateg,
    			amount: amount
    		}
    	}))
    },[perCategoryName])
    
  	
	return(
		
		<View title={'Category Breakdown'}>
			<Fragment>
				<h1>Category Breakdown</h1>
				<Row>
					<Col xs="6"  className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
							type="date"
							// placeholder="Search Record"
							value={fromDate}
							onChange={(e) => setFromDate(e.target.value)}
							required
							/>
						</Form.Group>
					</Col>

					<Col xs="6" className="mr-0 ml-0">
						<Form.Group>
							<Form.Control 
							type="date"
							// placeholder="Search Record"
							value={toDate}
							onChange={(e) => setToDate(e.target.value)}
							required
							/>
						</Form.Group>
					</Col>

				</Row>

				<PieChart categoryData={amountByCat} />
				
			</Fragment>
		</View>
		
	)
}

