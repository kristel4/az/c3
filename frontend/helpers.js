//to create a color, from a randomized hexadecimal value of a color 
const colorRandomizer = () => {
	return Math.floor(Math.random()*16777215).toString(16)
}
export {colorRandomizer};