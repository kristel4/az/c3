import React, {Fragment,useContext} from 'react';
import {Navbar, Nav} from 'react-bootstrap';
import Link from 'next/link';

import UserContext from '../UserContext';

export default function NavBar(){

	const {user} = useContext(UserContext);

	return (
		<Navbar bg="dark" variant="dark" expand="lg">
			<Link href="/">
				<a className="navbar-brand">Budget Tracker</a>
			</Link>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				
					{(user.id !== null) ?
						<Fragment>
						<Nav className="mr-auto">
							<Link href="/categories">
                                <a className="nav-link" role="button">
                                    Categories
                                </a>
                         	</Link>

                         	<Link href="/records">
                                <a className="nav-link" role="button">
                                    Records
                                </a>
                         	</Link>

                         	<Link href="/charts/monthly-expense">
                                <a className="nav-link" role="button">
                                    Monthly Expense
                                </a>
                         	</Link>

                         	<Link href="/charts/monthly-income">
                                <a className="nav-link" role="button">
                                    Monthly Income
                                </a>
                         	</Link>

                         	<Link href="/charts/balance-trend">
                                <a className="nav-link" role="button">
                                    Trend
                                </a>
                         	</Link>

                         	<Link href="/charts/category-breakdown">
                                <a className="nav-link" role="button">
                                    Breakdown
                                </a>
                         	</Link>
                         	</Nav>

                         	<Nav >
                         	<Link href="/logout" >
                                <a className="nav-link" role="button">
                                    Logout
                                </a>
                         	</Link>
                         	</Nav>
						</Fragment>
						:

						<Fragment>
							<Nav className="ml-auto">
							<Link href="/register">
                                <a className="nav-link" role="button">
                                    Register
                                </a>
                         	</Link>

                         	<Link href="/login">
                                <a className="nav-link" role="button">
                                    Login
                                </a>
                         	</Link>
                         	</Nav>
						</Fragment>
					}
				
			</Navbar.Collapse>
		</Navbar>
	)

}