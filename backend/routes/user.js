const express = require('express');
const router = express.Router();
const auth = require('../auth');
const UserController = require('../controllers/user');

//for registration of a user
router.post('/register', (req,res) => {
	UserController.register(req.body).then(result => res.send(result))
})

//to check email if it exists

router.post('/email-exists', (req,res) => {
	UserController.emailExists(req.body).then( result => res.send(result))
})

//login

router.post('/login', (req,res) => {
	UserController.login(req.body).then(result => res.send(result))
})

// to retrieve the details of the user from the database

router.get('/details', auth.verify, (req,res) => {
	const user = auth.decode(req.headers.authorization)
	UserController.get({userId: user.id}).then(user => res.send(user))
})

// for google verification of token

router.post('/verify-google-id-token', async(req,res) => {
	res.send(await UserController.verifyGoogleTokenId(req.body.tokenId))
})

//for adding a new category
router.post('/add-new-category', auth.verify, (req, res) => {

	const params = {
		userId: auth.decode(req.headers.authorization).id,
		categoryName: req.body.categoryName,
		categoryType: req.body.categoryType
	}
	UserController.addCategory(params).then(result => res.send(result))
})

//Finding user details 

router.get('/user-data', (req,res) => {

	const userId = {
		userId: auth.decode(req.headers.authorization).id
	}
	UserController.getUser(userId).then(result => res.send(result))
})

//for adding a new record

router.post('/add-new-record', auth.verify, (req,res) => {

	const params ={
		userId: auth.decode(req.headers.authorization).id,
		categoryType: req.body.categoryType,
		categoryList: req.body.categoryList,
		amount: req.body.amount,
		description:req.body.description
	}
	UserController.addRecord(params).then(result => res.send(result))
})

//for updating total balance of a user based on input value 
router.put('/update-total-balance', auth.verify, (req,res) => {
	const params = { 
		userId: auth.decode(req.headers.authorization).id,
		amount: req.body.amount,
		categoryType: req.body.categoryType
	}

	UserController.updateBalance(params).then(result => res.send(result))
})

module.exports = router;