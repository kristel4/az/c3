const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		require: [true, 'First name is required']
	},
	lastName:{
		type: String,
		require: [true, 'Last Name is required']
	},
	email: {
		type: String,
		require: [true, 'Email is required']
	},
	password: {
		type: String
	},
	mobileNo: {
		type: String
	},
	totalBalance: {
		type: Number,
		default: 0
	},
	loginType: {
		type: String,
		required: [true, 'Login type is required']
	},
	categories: [
		{
			name: {
				type: String
			},
			type: {
				type: String
			}
		}
	],
	records: [
		{
			categoryType: {
				type: String
			},
			categoryName: {
				type: String
			},
			amount: {
				type: Number,
				default: 0
			},
			description: {
				type: String
			},
			currentBalance: {
				type:Number,
				default:0
			},
			dateAdded: {
				type: Date,
				default: new Date()
			}
		}
	]

})

module.exports = mongoose.model('user', userSchema)