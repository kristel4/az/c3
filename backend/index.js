const express = require('express');
const nodemon = require('nodemon');
const mongoose = require ('mongoose');
const cors = require('cors');
require('dotenv').config();

const app = express();

const corsOptions = {
	origin: 'https://budget-tracker-host.vercel.app/',
	optionsSuccessStatus:200
}
app.use(cors(corsOptions));

const connectionString = process.env.DB_CONNECTION_STRING;
mongoose.connection.once('open', () => console.log("Now connected to MongoDB Atlas."));
mongoose.connect(connectionString,{
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false,
})

app.use(express.json());
app.use(express.urlencoded({extended:true}));

const userRoutes = require('./routes/user');

app.use('/api/users',userRoutes); 

const port = process.env.PORT;
app.listen(port,() => {console.log(`API is now online on ${port}`)})