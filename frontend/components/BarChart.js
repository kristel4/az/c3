import {useState, useEffect} from 'react';
import {colorRandomizer} from '../helpers';
import {Bar} from 'react-chartjs-2';

export default function BarChart({monthlyData}){
	console.log(monthlyData)
	const [months, setMonths] = useState([]);
	const [totalBalance, setTotalBalance] = useState([]);
	const [bgColors, setBgColors] = useState([]);



	useEffect(() => {

		if(monthlyData.length > 0){

			setMonths(monthlyData.map(element => element.month))


			setTotalBalance(monthlyData.map(element => element.balance))

			setBgColors(monthlyData.map(() =>`#${colorRandomizer()}`))
		}

	}, [monthlyData])

	console.log(months);
	console.log(totalBalance)


	const data = {
		labels: months,
		datasets: [{
			label: 'Total per month',
			backgroundColor: 'rgba(225,99,132,0.2)',
			borderColor: 'rgba(255,99,132,1)',
			borderWidth:1,
			hoverBackgroundColor: 'rgba(255,99,132,0.4)',
			hoverBorderColor: 'rgba(255,99,132,1)',
			data: totalBalance
		}]
	}
	
	return(
		<Bar data ={data} />
	)
}